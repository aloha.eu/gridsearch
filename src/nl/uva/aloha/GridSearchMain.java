package nl.uva.aloha;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import espam.datamodel.onnx.ONNX.ModelProto;
import io.jenetics.Chromosome;
import io.jenetics.Genotype;
import io.jenetics.util.IntRange;
import nl.uva.aloha.converters.GeneToOnnx;
import nl.uva.aloha.genetic.DualLayerChromosome;
import nl.uva.aloha.genetic.LayerGene;
import nl.uva.aloha.genetic.SimpleLayerChromosome;
import nl.uva.aloha.helpers.MongoDBTest;
import nl.uva.aloha.helpers.OnnxRegistry;
import nl.uva.aloha.helpers.SatelliteEvaluator;
import nl.uva.aloha.helpers.SearchResultCollector;

public class GridSearchMain 
{

	private static int[] convLayers = {1,2,3,4};
	private static int[] fcLayers = {1,2};
	private static int[] Mvalues = {8,16,32,64,128};
	private static int[] Jvalues = {50,100,150,200};
	public static String projectId = "";
	public static JSONObject configObject;
	
	
	static SatelliteEvaluator satEval ;
	public static void main(final String[] args) 
	{
		projectId = args[0];
		if((projectId==null) ||(projectId.isEmpty()))
			projectId = "a1";
		SatelliteEvaluator.projectId = projectId;
		
		String onnxfolderPath = args[1];
		if((onnxfolderPath==null) ||(onnxfolderPath.isEmpty()))
			onnxfolderPath = "/Users/sne/aloha_workspace/temponnx/";
		OnnxRegistry._onnxFolder = onnxfolderPath + "/" + projectId ;
		
		//System.out.println(projectId);
		try
		{
			
			satEval = new SatelliteEvaluator();
			String ConstraintJSON = MongoDBTest.retrieveConstraints(projectId).toString();
			JSONParser parser = new JSONParser(); 
			configObject = (JSONObject) parser.parse(ConstraintJSON);
	        
	        
			ArrayList<Genotype<LayerGene>> genotypes  = createDesignPoints();
			saveDesignPoints(genotypes);
			searchDesignSpace(genotypes, configObject);
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
	}
	
	
	public static void searchDesignSpace(ArrayList<Genotype<LayerGene>> genotypes, Object Constraints)
	{
		
		Iterator<Genotype<LayerGene>> it = genotypes.iterator();
		ArrayList<Genotype<LayerGene>> forAccuracyEval = new ArrayList<>();
		
		while(it.hasNext())
		{
			Genotype<LayerGene>gt = it.next();
			SearchResultCollector result = satEval.evaluateHardware(gt);
			System.out.println("ALOHA" + result.toString());
			if(result.energy < new Double(configObject.get("power_value").toString()))
				forAccuracyEval.add(gt);
		}
		
		it = forAccuracyEval.iterator();
		while(it.hasNext())
		{
			Genotype<LayerGene>gt = it.next();
			SearchResultCollector result = satEval.evaluateAccuracy(gt);
			System.out.println("ALOHA_accuracy_for" + result.toString());
		}
		
			
	}
	
	static public ArrayList<Genotype<LayerGene>> createDesignPoints()
	{
		ArrayList<Genotype<LayerGene>> genotypes = new ArrayList<>();
		
		for(int c=0; c < convLayers.length; c++)
		{
			for(int f=0; f < fcLayers.length; f++)	
			{
				for(int m=0; m < Mvalues.length; m++)
				{
					for(int j=0; j< Jvalues.length; j++)
					{
						ArrayList<Chromosome<LayerGene>> chromosomeList = new ArrayList<>();
						
						chromosomeList.add(SimpleLayerChromosome.of("dataI",1,2));
						
						int M = Mvalues[m];
						for(int i = 0; i<convLayers[c]; i++)
						{
							chromosomeList.add(DualLayerChromosome.of("Convolution:NonLinear",M,M,IntRange.of(2,3))); 
							chromosomeList.add(SimpleLayerChromosome.of("Pooling",1,2));
							M*=2;
							
						}
						
						int J = Jvalues[j];
						for(int i = 0; i<fcLayers[f]; i++)
						{
							chromosomeList.add(SimpleLayerChromosome.of("DenseBlock",J,J)); 
							J/=2;
						}
						
						
						chromosomeList.add(SimpleLayerChromosome.of("DenseBlock",10,10));
						chromosomeList.add(SimpleLayerChromosome.of("Softmax",10,10));
						chromosomeList.add(SimpleLayerChromosome.of("dataO",1,2));
						
						Genotype<LayerGene> gt = Genotype.of(chromosomeList);
						genotypes.add(gt);
					}
				}
			}
		}
		return genotypes;
	}
	
	
	static public void saveDesignPoints(ArrayList<Genotype<LayerGene>> genotypes)
	{
		//ArrayList<String> onnxModels = new ArrayList<>();
		
		Iterator<Genotype<LayerGene>> it = genotypes.iterator();
		while(it.hasNext())
		{
			Genotype<LayerGene>gt = it.next();
			GeneToOnnx convertor = new GeneToOnnx(gt);
			
			if(convertor.getNetwork().checkConsistency())
			{
				ModelProto model = convertor.convertToONNXModel();
				SearchResultCollector src = OnnxRegistry.saveOnnx(model, projectId);
				OnnxRegistry.getInstance().addEntry(System.identityHashCode(gt), src);
			}
			else
				System.out.println("Inconsistent network" + gt.toString());
		}
		//return onnxModels;
	}
	
	
	
}
