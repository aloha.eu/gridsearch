package nl.uva.aloha.converters;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.google.protobuf.ByteString;

import espam.datamodel.graph.cnn.BoundaryMode;
import espam.datamodel.graph.cnn.Layer;
import espam.datamodel.graph.cnn.Network;
import espam.datamodel.graph.cnn.Neuron;
import espam.datamodel.graph.cnn.connections.Connection;
import espam.datamodel.graph.cnn.neurons.cnn.CNNNeuron;
import espam.datamodel.graph.cnn.neurons.cnn.Convolution;
import espam.datamodel.graph.cnn.neurons.cnn.Pooling;
import espam.datamodel.graph.cnn.neurons.neurontypes.NonLinearType;
import espam.datamodel.graph.cnn.neurons.neurontypes.PoolingType;
import espam.datamodel.graph.cnn.neurons.simple.Data;
import espam.datamodel.graph.cnn.neurons.simple.DenseBlock;
import espam.datamodel.graph.cnn.neurons.simple.NonLinear;
import espam.datamodel.graph.sdf.datasctructures.Tensor;
import espam.datamodel.onnx.ONNX;
import espam.datamodel.onnx.ONNX.AttributeProto;
import espam.datamodel.onnx.ONNX.AttributeProto.AttributeType;
import espam.datamodel.onnx.ONNX.GraphProto;
import espam.datamodel.onnx.ONNX.ModelProto;
import espam.datamodel.onnx.ONNX.NodeProto;
import espam.datamodel.onnx.ONNX.OperatorSetIdProto;
import espam.datamodel.onnx.ONNX.TensorProto;
import espam.datamodel.onnx.ONNX.TensorProto.DataType;
import espam.datamodel.onnx.ONNX.TensorShapeProto;
import espam.datamodel.onnx.ONNX.TensorShapeProto.Dimension;
import espam.datamodel.onnx.ONNX.TypeProto;
import espam.datamodel.onnx.ONNX.ValueInfoProto;
import io.jenetics.Genotype;
import nl.uva.aloha.genetic.LayerGene;

public class GeneToOnnx {

	private Genotype<LayerGene> _genotype;
	private Network _network;
	
	private GraphProto _graphProto;
	
	public GeneToOnnx(Genotype<LayerGene> gt )
	{
		_genotype = gt;
		_network = GeneToNetwork.createNetworkFromGenotype(_genotype);
	}
	public GeneToOnnx(Network network)
	{
		_network = network;
		
	}
	
	public Network getNetwork()
	{
		return _network;
	}
	
	public ModelProto convertToONNXModel()
	{
		if(_network == null)
		{
			System.err.println("Network is not set properly. Nothing to convert");
			return null;
		}
		
		if(!_network.checkConsistency())
		{
			System.err.println("Network is not consistent. Cannot convert");
			return null;
		}
		
		 Network network = _network;
		
		 ModelProto.Builder _modelBuilder = ModelProto.newBuilder();
		 GraphProto.Builder _graphBuilder = GraphProto.newBuilder();
		
		
		//NODE creation
		Iterator<Layer> itr = network.getLayers().iterator();
		while(itr.hasNext()) 
		{
			Layer l = itr.next();
			Neuron currentNeuron = l.getNeuron();
			ArrayList<String> layerInputs = new ArrayList<String>(); 
			ArrayList<String> layerOutputs = new ArrayList<String>(); 
			//System.out.print(l.getName() + ":" + l.getInputFormat().toString());
			//Assuming input connections and output connections are same and Layer names are unique!!
			for(int i=0;i<l.getInputConnections().size();i++)
			{
				Connection cnxn = l.getInputConnections().get(i);
				if(cnxn.getSrc().equals(network.getInputLayer()))
					layerInputs.add("input_data");
				else
					layerInputs.add(cnxn.getSrcName() + "_" + cnxn.getDestName());
			}
			
			for(int i=0;i<l.getOutputConnections().size();i++)
			{
				Connection cnxn = l.getOutputConnections().get(i);
				layerOutputs.add(cnxn.getSrcName() + "_" + cnxn.getDestName());
			}
			
			if(l.equals(network.getInputLayer()))
			{
				Tensor outputFormat = new Tensor(l.getOutputFormat());
				outputFormat.addDimension(l.getNeuronsNum());
				//l.getName() + "_"+l.getOutputConnections().get(0).getDestName()
				ValueInfoProto valueProto = createInputProto("input_data", Tensor.reverse(outputFormat));
				_graphBuilder.addInput(valueProto);
				
				continue;
			}
			else if(l.equals(network.getOutputLayer()))
			{
				Tensor outputFormat = new Tensor(10);
				outputFormat.addDimension(l.getNeuronsNum());
				
				ValueInfoProto valueProto = createInputProto(l.getInputConnections().get(0).getSrc().getName() + "_" + l.getName(), Tensor.reverse(outputFormat));
				_graphBuilder.addOutput(valueProto);
				
				continue;
			}
			else if(currentNeuron instanceof Convolution)
			{
				String weightName = l.getName()+"weights";
				String biasName = l.getName()+"bias";
				layerInputs.add(weightName);
				layerInputs.add(biasName);
				
				//TODO: Assuming only one inputconnection as of now - later extend it to sum of all inputs
				int k = ((Convolution)(currentNeuron)).getKernelSize();
				int inputConnectionsCount = l.getInputFormat().getLastDimSize();
				Tensor weightFormat = new Tensor(l.getNeuronsNum(),inputConnectionsCount,k,k);
						
				_graphBuilder.addInput(createInputProto( weightName,weightFormat));
				_graphBuilder.addInput(createInputProto(biasName, new Tensor(l.getNeuronsNum())) );
				
				_graphBuilder.addInitializer(createRandomWeights(weightName, weightFormat));
				_graphBuilder.addInitializer(createRandomWeights(biasName, new Tensor(l.getNeuronsNum())) );
				
				
			}
			else if(currentNeuron instanceof DenseBlock)
			{
				String weightName = l.getName()+"weights";
				String biasName = l.getName()+"bias";
				layerInputs.add(weightName);
				layerInputs.add(biasName);
				
				//Our Format is  W x H x N1 x N2,    N2 - Number of neurons in previous layer. N1 is ignored in gemm layer, But i do not know why yet ~Dolly 
				//OR it vanishes in one of the previous layers.. Conv/Relu/Pool - one of them absorbs it i think :)
				//ONNX Tensor order: N x C x H x W,     N- batch size, C- Channels
				Tensor ip = l.getInputFormat();
				int inputSize = ip.getDimSize(0);
				switch(ip.getDimensionality())
				{
		
				case 2: inputSize*=ip.getDimSize(1);
						break;
				case 3: 		
				case 4: inputSize*=(ip.getDimSize(1)*ip.getLastDimSize());
						break;
				}
				
				Tensor weightFormat = new Tensor(l.getOutputFormat().getLastDimSize(),inputSize );
				
				/*if((currentNeuron instanceof Gemm)&&((Gemm)(currentNeuron)).isBTransposed())
					weightFormat = Tensor.reverse(weightFormat);*/
				
				_graphBuilder.addInput(createInputProto( weightName, weightFormat));
				_graphBuilder.addInput(createInputProto(biasName, new Tensor(l.getOutputFormat().getLastDimSize())) );
				
				_graphBuilder.addInitializer(createRandomWeights(weightName, weightFormat));
				_graphBuilder.addInitializer(createRandomWeights(biasName, new Tensor(l.getOutputFormat().getLastDimSize())) );
				
			}
			
			
		
			
			//I think there should not be data layers other than input/output - but not sure so an extra check ~Dolly
			if(!(currentNeuron instanceof Data))
				_graphBuilder.addNode(layerToOnnxNode(l,layerInputs,layerOutputs));
		}
	
		
		
		
		
		
		
		
		_modelBuilder.setProducerName("ALOHA");
		_modelBuilder.addOpsetImport(setopImportSet());
		_modelBuilder.setIrVersion(ONNX.Version.IR_VERSION.getNumber());
		
		_graphBuilder.setName("GAtest"+new Date().getTime());
		_graphProto = _graphBuilder.build();
		_modelBuilder.setGraph(_graphProto);
		
		
		
		
		return(_modelBuilder.build());
		
	}
	

	
	private OperatorSetIdProto setopImportSet()
	{
		OperatorSetIdProto.Builder _opSetBuilder = OperatorSetIdProto.newBuilder();
		_opSetBuilder.setDomain("");
		_opSetBuilder.setVersion(3);
		return _opSetBuilder.build();
	}
	
	private ValueInfoProto createInputProto(String name, Tensor inputTensorDims)
	{
		ValueInfoProto.Builder inputBuilder = ValueInfoProto.newBuilder();
		inputBuilder.setName(name);
		
		
		TypeProto.Builder typeBuilder = TypeProto.newBuilder();
		
		TensorShapeProto.Builder shapeBuilder = TensorShapeProto.newBuilder();
		
		//inputTensorDims = Tensor.reverse(inputTensorDims);
		for(int i=0; i< inputTensorDims.getDimensionality(); i++)
		{
			Dimension.Builder d =  Dimension.newBuilder();
			d.setDimValue(inputTensorDims.getDimSize(i));
			shapeBuilder.addDim(d.build());
		}
		TypeProto.Tensor.Builder tensorBuilder =  TypeProto.Tensor.newBuilder();
		tensorBuilder.setElemType(DataType.FLOAT);
		tensorBuilder.setShape(shapeBuilder.build());
	
		
		typeBuilder.setTensorType(tensorBuilder.build());
		inputBuilder.setType(typeBuilder.build());
		return inputBuilder.build();
		
	}
	
	@SuppressWarnings("unused")
	private TensorProto createZeroWeights(String name, Tensor inputTensorDims)
	{
		TensorProto.Builder _builder = TensorProto.newBuilder();
		
		_builder.setName(name);
		int totalValues =1;
		
		for(int i=0; i< inputTensorDims.getDimensionality(); i++)
		{
			int dim = inputTensorDims.getDimSize(i);
			_builder.addDims(dim);
			totalValues*=dim;
		}
		_builder.setDataType(DataType.FLOAT);
		
		ByteBuffer bb = ByteBuffer.allocate(totalValues*4);
		_builder.setRawData(ByteString.copyFrom(bb, bb.capacity()));
		
		return _builder.build();
	}
	
	
	
	private TensorProto createRandomWeights(String name, Tensor inputTensorDims)
	{
		TensorProto.Builder _builder = TensorProto.newBuilder();
		
		_builder.setName(name);
		int totalValues =1;
		
		for(int i=0; i< inputTensorDims.getDimensionality(); i++)
		{
			int dim = inputTensorDims.getDimSize(i);
			_builder.addDims(dim);
			totalValues*=dim;
		}
		_builder.setDataType(DataType.FLOAT);
		
		ByteBuffer bb = ByteBuffer.allocate(totalValues*4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		for(int j=0;j<totalValues;j++)
		{
			float a = new Float(Math.random()*0.01);
			if(Math.random() < 0.25)
				a*=-1;
			bb.putFloat(j*4, a);
		}
		_builder.setRawData(ByteString.copyFrom(bb, bb.capacity()));
		
		return _builder.build();
	}
	
	
	//TODO Add inputs and outputs list
	private NodeProto layerToOnnxNode(Layer l, ArrayList<String> inputs, ArrayList<String> outputs)
	{
		
		NodeProto.Builder _nodeBuilder = NodeProto.newBuilder();
		Neuron currentNeuron = l.getNeuron();
		_nodeBuilder.setName(l.getName());
		
		for(int inp =0; inp<inputs.size(); inp++)
		{ 
			_nodeBuilder.addInput( inputs.get(inp));
		}
		
		for(int outp=0; outp<outputs.size(); outp++)
		{ 
			_nodeBuilder.addOutput( outputs.get(outp));
		}
		
		if(currentNeuron instanceof CNNNeuron)
		{
			
			if(currentNeuron instanceof Pooling)
			{
				//Neuron name is type of pooling
				//layername is nodename
				
				
				if(currentNeuron.getName().contains(PoolingType.AVGPOOL.toString()))
					_nodeBuilder.setOpType("AveragePool");
				else if(currentNeuron.getName().contains(PoolingType.GLOBALAVGPOOL.toString()))
					_nodeBuilder.setOpType("GlobalAveragePool");
				else if(currentNeuron.getName().contains(PoolingType.GLOBALLPPOOL.toString()))
					_nodeBuilder.setOpType("GlobalLpPool");
				else if(currentNeuron.getName().contains(PoolingType.GLOBALMAXPOOL.toString()))
					_nodeBuilder.setOpType("GlobalMaxPool");
				
				//Default is MaxPool
				else //(currentNeuron.getName().contains(PoolingType.MAXPOOL.toString()))
					_nodeBuilder.setOpType("MaxPool");
				
			}
			else if(currentNeuron instanceof Convolution)
			{
				_nodeBuilder.setOpType("Conv");
			}
			
			_nodeBuilder.addAttribute(padsToAttributes(l.getPads()));
			_nodeBuilder.addAttribute(kernelShapeToAttributes(  ((CNNNeuron)(currentNeuron)).getKernelSize()  ));
			_nodeBuilder.addAttribute(strideToAttributes(  ((CNNNeuron)(currentNeuron)).getStride()  ));
			//ONNX says use explicit padding instead of auto pad!! it is for legacy use only
			//TODO: CONVERT auto pad in our code to explicit padding..
			_nodeBuilder.addAttribute(boundaryToAttributes( ((CNNNeuron)(currentNeuron)).getBoundaryMode() ));
		
		}
		
		
		//GEMM has alpha/beta values. Y== alpha*(A*B) + Beta*C. A,B,C are matrices. alpha,beta are just one number(float)
		//MatMul is only matrix multiplication
		/*else if(currentNeuron instanceof Gemm)
		{
			Gemm gemm = (Gemm)currentNeuron;
			_nodeBuilder.setOpType("Gemm");
			//broadcast has been removed from latest operators list
			//_nodeBuilder.addAttribute(intValueToAttribute("broadcast", (gemm.isBroadcast()?1:0) ));
			_nodeBuilder.addAttribute(intValueToAttribute("transB", (gemm.isBTransposed()?1:0)  ));
			_nodeBuilder.addAttribute(intValueToAttribute("transA", (gemm.isATransposed()?1:0)  ));
			_nodeBuilder.addAttribute(floatValueToAttribute("alpha", gemm.getAlpha()));
			_nodeBuilder.addAttribute(floatValueToAttribute("beta", gemm.getBeta()));
		}*/
		
		else if(currentNeuron instanceof DenseBlock)
		{
			int mn=1;
			_nodeBuilder.setOpType("Gemm");
			_nodeBuilder.addAttribute(floatValueToAttribute("alpha", new Float(1.0)));
			_nodeBuilder.addAttribute(floatValueToAttribute("beta", new Float(1.0)));
			_nodeBuilder.addAttribute(intValueToAttribute("transB", mn));
		}
		else if(currentNeuron instanceof NonLinear)
		{
			if(currentNeuron.getName().contains(NonLinearType.SOFTMAX.toString()))
				_nodeBuilder.setOpType("Softmax");
			else if(currentNeuron.getName().contains(NonLinearType.SOFTPLUS.toString()))
				_nodeBuilder.setOpType("Softplus");
			else if(currentNeuron.getName().contains(NonLinearType.THN.toString()))
				_nodeBuilder.setOpType("Tanh");
			else if(currentNeuron.getName().contains(NonLinearType.SIGM.toString()))
				_nodeBuilder.setOpType("Sigmoid");
			else if(currentNeuron.getName().contains(NonLinearType.SELU.toString()))
				_nodeBuilder.setOpType("Selu");
			else if(currentNeuron.getName().contains(NonLinearType.LeakyReLu.toString()))
				_nodeBuilder.setOpType("LeakyRelu");
			
			//Default non-linear neuron to Relu for now
			else //(currentNeuron.getName().contains(NonLinearType.ReLU.toString()))
				_nodeBuilder.setOpType("Relu");
			 
		}
		
		
		return _nodeBuilder.build();
	}
	
	
	private AttributeProto padsToAttributes(int[] pads)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName("pads");
		_attBuilder.setType(AttributeType.INTS);
		
		if(pads== null)
		{
			pads = new int[] {0,0,0,0};
		}
		
		for(int i=0;i<pads.length;i++)
           _attBuilder.addInts(pads[i]);
		
		return _attBuilder.build();
		
	}
	
	private AttributeProto kernelShapeToAttributes(int kernelSize)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName("kernel_shape");
		_attBuilder.setType(AttributeType.INTS);
		
		//Add twice - From our format to onnx
        _attBuilder.addInts(kernelSize);
        _attBuilder.addInts(kernelSize);
        
		return _attBuilder.build();
		
	}
	
	
	private AttributeProto strideToAttributes(int strideSize)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName("strides");
		_attBuilder.setType(AttributeType.INTS);
		
		//Add twice - From our format to onnx
        _attBuilder.addInts(strideSize);
        _attBuilder.addInts(strideSize);
        
		return _attBuilder.build();
		
	}
	
	private AttributeProto boundaryToAttributes(BoundaryMode bm)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName("auto_pad");
		_attBuilder.setType(AttributeType.STRING);
		
		//Boundary mode is set as auto_pad in onnx. in our format there is only SAME - setting it to SAME_UPPER in onnx for now. 
		if(bm.equals(BoundaryMode.VALID))
			_attBuilder.setS(ByteString.copyFromUtf8("VALID"));
		else
			_attBuilder.setS(ByteString.copyFromUtf8("SAME"));
			
		return _attBuilder.build();
		
	}

	private AttributeProto intValueToAttribute(String attributeName, int value)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName(attributeName);
		_attBuilder.setType(AttributeType.INT);
		_attBuilder.setI(value);
		return _attBuilder.build();
		
	}
	
	private AttributeProto floatValueToAttribute(String attributeName, float value)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName(attributeName);
		_attBuilder.setType(AttributeType.FLOAT);
		_attBuilder.setF(value);
		return _attBuilder.build();
		
	}
	
	@SuppressWarnings("unused")
	private AttributeProto StringValueToAttribute(String attributeName, String text)
	{
		AttributeProto.Builder _attBuilder = AttributeProto.newBuilder();
		_attBuilder.setName(attributeName);
		_attBuilder.setType(AttributeType.STRING);
		_attBuilder.setS(ByteString.copyFromUtf8(text));
		return _attBuilder.build();
		
	}
	
	
}



/** NOTES
 *  
 *  
 * 
 * */
