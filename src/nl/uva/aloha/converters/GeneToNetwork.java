package nl.uva.aloha.converters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import espam.datamodel.graph.cnn.Layer;
import espam.datamodel.graph.cnn.Network;
import espam.datamodel.graph.cnn.neurons.simple.DenseBlock;
import espam.datamodel.graph.sdf.datasctructures.Tensor;
import io.jenetics.AbstractChromosome;
import io.jenetics.Chromosome;
import io.jenetics.Genotype;
import nl.uva.aloha.genetic.LayerGene;

public class GeneToNetwork 
{
	 final static Tensor INPUT_DATA_CIFAR_SHAPE = new Tensor(32,32,3);
	
	
	//DOES NOT SPECIFY CONNECTION TYPE 
	//TODO: Add connectionParameters
	private static Network createNetworkFromLayerGenes(List<?> layers)
	{
		Network network = new Network();
		int i =0;
		try
		{
				if(layers.get(0) instanceof LayerGene )
				{
					network.stackLayerTop("input", ((LayerGene)layers.get(0)).getAllele().getNeuron(),1);
				}
				
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
		
		
		for(i=1;i<layers.size();i++) 
		{
			if(layers.get(i) instanceof LayerGene )
			{
				Layer la = ((LayerGene)layers.get(i)).getAllele();
				try 
				{
					//Layer nextLayer = ((LayerGene)layers.get(i+1)).getAllele();
					
					if((i<layers.size()-1)&& (la.getNeuron() instanceof DenseBlock) )
					{
						Layer nextLayer = ((LayerGene)layers.get(i+1)).getAllele();
						if((nextLayer.getName().contains("softmax")))
						{	
							DenseBlock db = (DenseBlock)la.getNeuron();
							db.setNeuronsNum(nextLayer.getNeuronsNum());
						}
					}
					
					network.stackLayer(la.getName(),la.getNeuron(),la.getNeuronsNum(),la.getPads());
					
				}
				catch(Exception e) {
					System.err.println(e.getMessage());
				}
			}
			
			
		}
		
		network.setInputLayer(network.getLayers().firstElement());
		network.setOutputLayer(network.getLayers().lastElement());
        
		try {
		//TODO: Input data is set for CIFAR here. update this to reflect other datasets.
		
				Tensor inputDataShapeExample = INPUT_DATA_CIFAR_SHAPE;
				network.setDataFormats(inputDataShapeExample);
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		return network;
	}
	
	
	public static Network createNetworkFromGenotype(Genotype<LayerGene> gt)
	{ 
		Network network = null;
		ArrayList<LayerGene> layerGenes = new ArrayList<>();
		
		for (Iterator<Chromosome<LayerGene>> i = gt.iterator(); i.hasNext(); ) 
		{
			AbstractChromosome<LayerGene> ac = (AbstractChromosome<LayerGene>)(i.next());
			for (Iterator<LayerGene> j = ac.iterator(); j.hasNext(); ) 
				layerGenes.add(j.next());
			
		}
		network =createNetworkFromLayerGenes(layerGenes);
		
		//Tensor inputDataShapeExample = new Tensor(32,32,3);
		//network.setDataFormats(inputDataShapeExample);
		
		return network;
	}
}
