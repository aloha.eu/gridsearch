package nl.uva.aloha.genetic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import espam.datamodel.graph.cnn.BoundaryMode;
import espam.datamodel.graph.cnn.Layer;
import espam.datamodel.graph.cnn.Neuron;
import espam.datamodel.graph.cnn.connections.ConnectionType;
import espam.datamodel.graph.cnn.neurons.cnn.Convolution;
import espam.datamodel.graph.cnn.neurons.cnn.Pooling;
import espam.datamodel.graph.cnn.neurons.neurontypes.DataType;
import espam.datamodel.graph.cnn.neurons.neurontypes.NonLinearType;
import espam.datamodel.graph.cnn.neurons.neurontypes.PoolingType;
import espam.datamodel.graph.cnn.neurons.simple.Data;
import espam.datamodel.graph.cnn.neurons.simple.DenseBlock;
import espam.datamodel.graph.cnn.neurons.simple.NonLinear;
import io.jenetics.Gene;
import io.jenetics.util.RandomRegistry;

public class LayerGene 
		implements Gene<Layer,LayerGene>, Serializable
{
	
	
	
	
	private static final long serialVersionUID = 8077673452944666485L;

	protected Layer _layer;
	protected int _minNumNeurons = 1;
	protected int _maxNumNeurons = 20; //Default values if not set explicitly.//TODO: Dense block should have a lot more number of neurons
	protected String _layerType;
	
	//@SuppressWarnings("unused")
	//private ConnectionType _cnxnTypeToNextLayer;
	
	
	private static ArrayList<String> LayerTypes = new ArrayList<String>(Arrays.asList( "Convolution", 
															                    "Pooling",
															                    "NonLinear",
															                    "DenseBlock",
															                    "DataI",
															                    "DataO"));
			
	public LayerGene(Layer l)
	{
		_layer = l; //TODO : cloning throws error - check what is going on
		_layerType = l.getNeuron().getNeuronType().toString();
		_maxNumNeurons = l.getNeuronsNum()+1;
		_minNumNeurons = 1;
		
		//_cnxnTypeToNextLayer = cnxn; //ConnectionType is enum - so no need to clone. 
	}
	
	
	public LayerGene(String layerType)
	{
		_layerType = layerType;
		_layer = createLayerOfType(layerType, getRandomNumNeuron());
	}
	
	public LayerGene(String layerType,int numberOfNeurons)
	{
		_layerType = layerType;
		_layer = createLayerOfType(layerType, numberOfNeurons);
	}
	
	public LayerGene(String layerType,int minNeurons, int maxNeurons)
	{
		_layerType = layerType;
		_maxNumNeurons = maxNeurons;
		_minNumNeurons = minNeurons;
		_layer = createLayerOfType(layerType, getRandomNumNeuron());
	}
	
	private int getRandomNumNeuron()
	{
		if(_minNumNeurons == _maxNumNeurons)
			return _minNumNeurons;
		
		return RandomRegistry.getRandom().nextInt(_maxNumNeurons - _minNumNeurons) + _minNumNeurons; 
	}
	
	
	public LayerGene(Layer l,int minNeurons, int maxNeurons)
	{
		_layer = l; //TODO : cloning throws error - check what is going on - UPDATE: cloned layer is being sent here now
		_layerType = l.getNeuron().getNeuronType().toString();
		_maxNumNeurons = maxNeurons;
		_minNumNeurons = minNeurons;
	}
	
	public LayerGene()
	{
		String randomLayerType = LayerTypes.get(RandomRegistry.getRandom().nextInt(LayerTypes.size()-1)); // -1 to size to avoid a new data layer
		_layerType = randomLayerType;
		_layer = createLayerOfType(randomLayerType, getRandomNumNeuron());
	}
	
	
	
	private Layer createLayerOfType(String layerType, int numberOfNeurons) 
	{
		Layer l = null;
		switch (layerType)
		{
			case "dataI":  	Data inputData = new Data(DataType.INPUT);
						   	l = new Layer("input",inputData,1);
				break;
				
			case "dataO":  	Data outData = new Data(DataType.OUTPUT);
			   				l = new Layer("output",outData,1);
			   	break;
	
			case "Convolution":
							int k = 3; //kernel size
							Convolution cnv = new Convolution(k, BoundaryMode.VALID); //KernelSize, boundary, stride(default=1)
							l = new Layer("conv"+UUID.randomUUID().toString().replace("-", ""),cnv,numberOfNeurons);// +1 because it generates 0 as well
							l.setPads(k/2,k/2,k/2,k/2);
							//_cnxnTypeToNextLayer = ConnectionType.ONETOALL;
				break;
			
			case "Pooling": Pooling pool = new Pooling(PoolingType.MAXPOOL,2); //Stride=2
							l = new Layer("pool"+UUID.randomUUID().toString().replace("-", ""),pool,numberOfNeurons);
							//_cnxnTypeToNextLayer = ConnectionType.ONETOONE;
				break;
		
			case "NonLinear": NonLinear relu = new NonLinear(NonLinearType.ReLU);
							l = new Layer("relu"+UUID.randomUUID().toString().replace("-", ""),relu,numberOfNeurons);
							//_cnxnTypeToNextLayer = ConnectionType.ONETOONE;
				break;
				
				// Dense Layer is considered to be one block element ? why?
			case "DenseBlock": DenseBlock dense = new DenseBlock(numberOfNeurons);
							l = new Layer("dense1"+UUID.randomUUID().toString().replace("-", ""), dense,1);
							//_cnxnTypeToNextLayer = ConnectionType.ALLTOALL;
				break;
				
			case "Softmax":  NonLinear softmax = new NonLinear( NonLinearType.SOFTMAX);
							l = new Layer("softmax", softmax,numberOfNeurons);
							//_cnxnTypeToNextLayer = ConnectionType.ALLTOALL;
				break;
		}
		
		return l;
	}

	
	public LayerGene clone()
	{
		Layer newLayer = new Layer(getLayer().getName(), Neuron.copyNeuron(getLayer().getNeuron()), getLayer().getNeuronsNum());
		return new LayerGene(newLayer,_minNumNeurons,_maxNumNeurons);
	}
	
	public LayerGene mutate(double rate)
	{
		//Number of neurons change here - 
		LayerGene lg = this.clone();
		Float randomNextProb = RandomRegistry.getRandom().nextFloat();
		if(lg.getLayer().getNeuron() instanceof Convolution)
		{
			if(randomNextProb<=0.9)
			{
				int originalNeuronsNum = lg._layer.getNeuronsNum();
				//If rate = 0.1 We want final rate to be between 0.9 and 1.1 to reflect 10% up or down changes
				// 1.0 + rate - random number between <0.0,1.0> * rate*2 = 1.1 - random number between 0.0 and 0.2. 
				
				float finalRate = (float)(1.0 + rate - (RandomRegistry.getRandom().nextDouble()*rate*2));
				int modifiedNeuronsNum = Math.round(originalNeuronsNum*finalRate);
				lg._layer.setNeuronsNum(modifiedNeuronsNum);
			}
			else //if((randomNextProb>0.8)&&(randomNextProb<0.97))) //17% of cases change kernel size
			{
				Convolution cnv = (Convolution)lg.getLayer().getNeuron();
				int k = cnv.getKernelSize();
				
				if((randomNextProb<0.95) && (k<5))
					cnv.setKernelSize(k+2);
				
				else if((randomNextProb>=0.95) && (k>2))
					cnv.setKernelSize(k-2);
			}
			/*else 
			{
				//97,98,99 - so possible stride values are 1,2,3. 
				int stride = 100 - new Float(randomNextProb*100).intValue();
				((Convolution)(lg._layer.getNeuron())).setStride(stride);
			}*/
		}
		else if((lg.getLayer().getNeuron() instanceof NonLinear) && !(lg.getLayer().getNeuron().getName().contains(NonLinearType.SOFTMAX.toString())))
		{
				
			if(randomNextProb<=0.05)
				lg.getLayer().getNeuron().setName(NonLinearType.LeakyReLu.toString());
			else if(randomNextProb<=0.1)
				lg.getLayer().getNeuron().setName(NonLinearType.SELU.toString());
			else if(randomNextProb<=0.15)
				lg.getLayer().getNeuron().setName(NonLinearType.THN.toString());
			else if(randomNextProb<=0.2)
				lg.getLayer().getNeuron().setName(NonLinearType.SIGM.toString());
			else lg.getLayer().getNeuron().setName(NonLinearType.ReLU.toString());
				
						
		}
		else if(lg.getLayer().getNeuron() instanceof DenseBlock)
		{
			DenseBlock db = (DenseBlock)lg._layer.getNeuron();
			int originalNeuronsNum = db.getNeuronsNum();
			float finalRate = (float)(1.0 + rate - (RandomRegistry.getRandom().nextDouble()*rate*2));
			int modifiedNeuronsNum = Math.round(originalNeuronsNum*finalRate);
			db.setNeuronsNum(modifiedNeuronsNum);
		}
		
		/*else if(lg.getLayer().getNeuron() instanceof Pooling)
		{
			int typeNums = new Float(PoolingType.values().length*randomNextProb).intValue();
			((Pooling)lg.getLayer()).setNeuronType(PoolingType.values()[typeNums]);
		}*/
		
		
		return lg;
		
	}
	public void changeLayerSize(int neuronsNum)
	{
		_layer.setNeuronsNum(neuronsNum);
	}
	
	
	
	/*public void changeKernel(int kernelSize)
	{
		changeKernel(kernelSize, BoundaryMode.VALID, 1);
	}
	
	public void changeKernel(int kernelSize, int kernelStride)
	{
		changeKernel(kernelSize, BoundaryMode.VALID, kernelStride);
	}
	
	public void changeKernelStride(int kernelStride)
	{
		changeKernel(_layer.getNeuron().get, BoundaryMode.VALID, kernelStride);
	}
	
	public void changeKernel(int kernelSize, BoundaryMode boundaryMode)
	{
		changeKernel(kernelSize, boundaryMode, 1);
	}
	
	public void changeKernel(int kernelSize,BoundaryMode boundaryMode,int kernelStride)
	{
		if(_layer.getNeuron() instanceof Convolution)
		{
			_layer.setNeuron(new Convolution(kernelSize,boundaryMode,kernelStride));
		}
	}
	*/
	/*public void changeConnectionTypeTo(ConnectionType connectionType)
	{
		_cnxnTypeToNextLayer = connectionType;
	}*/
	
	
	@Override
	public String toString() {
		return _layer.toString() + " : {" + _layer.getNeuron().toString() + "}";
	}

	@Override
	public boolean isValid() 
	{		
		return (_layer!=null);
	}

	@Override
	public Layer getAllele() 
	{
		return _layer;
	}

	public Layer getLayer() 
	{
		return _layer;
	}
	
	@Override
	public LayerGene newInstance() 
	{
		return new LayerGene();
	}

	@Override
	public LayerGene newInstance(Layer layer) 
	{
	
		return new LayerGene(layer);
	}

	public int getMinNumNeurons() {
		return _minNumNeurons;
	}

	public void setMinNumNeurons(int minNumNeurons) {
		_minNumNeurons = minNumNeurons;
	}

	public int getMaxNumNeurons() {
		return _maxNumNeurons;
	}

	public void setMaxNumNeurons(int maxNumNeurons) {
		_maxNumNeurons = maxNumNeurons;
	}

	public String getLayerType() {
		return _layerType;
	}

	public void setLayerType(String _layerType) {
		this._layerType = _layerType;
	}
	
}
