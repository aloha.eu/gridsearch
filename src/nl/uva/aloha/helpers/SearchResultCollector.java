package nl.uva.aloha.helpers;

public class SearchResultCollector 
{
	
	//public Genotype<LayerGene> genotype;
	public String id;
	public String onnxName;
	public Double accuracy;
	public Double memory;
	public Double energy;
	public Double performance;
	public int processors; //THIS is originally int - convert to double to use this class! 

	public String toString()
	{
		return  "Onnx" + onnxName +
				"-accuracy:" + accuracy +
				"-memory:" + memory +
				"-energy:" + energy +
				"-proc:" + processors;
				
	}
}
