package nl.uva.aloha.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ConfigurationFromDB 
{
	public String _pythonscriptFolder = "./";
	
	public ConfigurationFromDB(String pythonLocation)
	{
		if(pythonLocation == "")
			pythonLocation = "/usr/local/bin/python3";
				
		 String[] cmd = new String[3];
	        cmd[0] = pythonLocation;
	        cmd[1] = _pythonscriptFolder + "config"; //TODO REPACE WITH SCRIPT NAME
	      
	        
	        try
	        {
	        	 Runtime rt = Runtime.getRuntime();
	             Process pr = rt.exec(cmd);
	             
	             BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
	             String line = "";
	             String pythonScriptResult = "";
	             while((line = bfr.readLine()) != null) {
	                 pythonScriptResult+=line;
	             }
	             
	             JSONParser parser = new JSONParser(); 
	             JSONObject configObject = (JSONObject) parser.parse(pythonScriptResult);

	             	 

	        }
	        catch(Exception e)
	        {
	        	System.err.println(e);
	        }
	        
	}
}
