package nl.uva.aloha.helpers;

/******************************************************************************
 *

 *  MongoDB test
 *
 * mongo-java-driver-3.10.2
 * 
 ******************************************************************************/

import static com.mongodb.client.model.Filters.eq;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class MongoDBTest {

    public static String retrieveConstraints (String project_id)
    {
    	((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);
		
    	MongoClient mongoClient = null;
    	try
    	{
    		mongoClient = new MongoClient("localhost", 27017);
            
            // get a Database
            MongoDatabase database = mongoClient.getDatabase("aloha");
            
            //get a collection
            MongoCollection<Document> collection = database.getCollection("project_constraints");
            
            // filter the collection by project field and if many results are returned select the first
            Document myDoc = collection.find(eq("project", project_id)).first();
            mongoClient.close();
            return myDoc.toJson();
    	}
    	catch(Exception e)
    	{
    		System.err.println(e.getMessage());
    		
    		return "{\"_id\": {\"$oid\": \"5cb5993075f2c5000b935d36\"}, \"creation_date\": {\"$date\": 1555405104890}, \"modified_date\": {\"$date\": 1555405104890}, \"power_value\": 0.5, \"power_priority\": 2, \"security_value\": 3, \"security_priority\": 1, \"execution_time_value\": 4, \"execution_time_priority\": 1, \"accuracy_value\": 3, \"accuracy_priority\": 1, \"project\": \"5cb5993075f2c5000b935d33\"}";   
    	}
    	finally
    	{
    		if(mongoClient!=null)
    			mongoClient.close();
    	}
        
    }
    
    public static ObjectId createAlgorithmConfiguration (String project_id, String onnxPath){
    	((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);
		
        Document algo = new Document("project", project_id)
                .append("onnx", onnxPath)
                .append("pytorch_net", "")
                .append("onnx_trained", "")
                .append("pytorch_trained", "")
                .append("onnx_trained", "")
                .append("training_accuracy", 0)
                .append("training_loss", 0)
                .append("validation_accuracy", Math.random())
                .append("validation_loss", 0)
                .append("training_log", "TODO: replace with array")
                .append("execution_time", "TODO: replace with array")
                .append("rpi_onnx_path", "")
                .append("performance", Math.random())
                .append("energy", Math.random())
                .append("processors", 2.0)
                .append("memory", Math.random())
                .append("sec_level", "")
                .append("sec_value", 0.0)
                .append("sec_curve", "TODO: replace with dict")
                .append("training_img_path", "")
                .append("power_perf_eval_img_path", "")
                .append("rpi_img_path", "")
                .append("security_img_path", "")
                .append("training_log_path", "")
                .append("power_perf_eval_log_path", "")
                .append("rpi_log_path", "")
                .append("security_log_path", "");
        
        
        
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        
        // get the Database
        MongoDatabase database = mongoClient.getDatabase("aloha");
        
        //get a collection
        MongoCollection<Document> collection = database.getCollection("algorithm_configuration");
        
        collection.insertOne(algo);
      
        ObjectId id = (ObjectId) algo.get( "_id" );
        
        System.out.println(id);
        
        return id;
        
    }

    public static String retrieveAlgorithmConfiguration (String algorithm_id)
    {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        
        // get a Database
        MongoDatabase database = mongoClient.getDatabase("aloha");
        
        //get a collection
        MongoCollection<Document> collection = database.getCollection("algorithm_configuration");
        
        // filter the collection by project field and if many results are returned select the first
        Document myDoc = collection.find(eq("project", algorithm_id)).first();
        return myDoc.toJson();
    }
    
    public static String retrieveAlgorithmConfiguration (String algorithm_id, String field)
    {
    	try
    	{
    		((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);
    		 MongoClient mongoClient = new MongoClient("localhost", 27017);
    	        
    	        // get a Database
    	        MongoDatabase database = mongoClient.getDatabase("aloha");
    	        
    	        //get a collection
    	        MongoCollection<Document> collection = database.getCollection("algorithm_configuration");
    	        
    	        // filter the collection by project field and if many results are returned select the first
    	        Document myDoc = collection.find(eq("_id", new ObjectId(algorithm_id))).first();
    	        String res= myDoc.get(field).toString();
    	        
    	        //System.out.println(field + "MongoDBreturns:" + res);
    	        mongoClient.close();
    	        return res;
    	}
    	catch(Exception e)
    	{
    		System.out.println("MONGODBERROR:" + e.getMessage());
    		return "0.0";
    	}
       
    }
    

    
    public static void main(String[] args) {
        
        System.out.println("########################");
        System.out.println("##### MongoDB Test #####");
        System.out.println("########################\n\n");
        
        //connect to the mongo db server
        System.out.println("Connecting to the server...");
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        
        // get a Database
        MongoDatabase database = mongoClient.getDatabase("aloha");
        
        
        // list all the collections, i.e. tables, in the DB
        MongoIterable<String> colls = database.listCollectionNames();
        for (String s : colls) {
          System.out.println(s);
        }
        
        //get a collection
        MongoCollection<Document> collection = database.getCollection("project_constraints");
        
        // print the number of documents in the collection, i.e. rows of the table
        System.out.println(collection.countDocuments());
        
        // get the first element of the collection
        Document myDoc = collection.find().first();
        System.out.println(myDoc.toJson());
        
        
        
        System.out.println(retrieveConstraints("5cb5993075f2c5000b935d33"));
        
        //createAlgorithmConfiguration("5cb5993075f2c5000b935d33");

    }

}

