package nl.uva.aloha.helpers;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import org.bson.types.ObjectId;

import espam.datamodel.onnx.ONNX.ModelProto;
import espam.utils.fileworker.ONNXFileWorker;

public class OnnxRegistry 
{
	private static final OnnxRegistry singletonRegistryInstance = new OnnxRegistry();;
	private static HashMap<Integer, SearchResultCollector> Register;
	
	//public static String _onnxWorkspacePath = "/Users/sne/aloha_workspace/onnx/";
	public static String _onnxRootFolder;//= _onnxWorkspacePath +  new Date().getTime() + "/";
	public static String _onnxFolder;//= _onnxWorkspacePath +  new Date().getTime() + "/";
	
	
	private static long idCounter = 1;
	
	
	
	
	private OnnxRegistry()
	{
		Register = new HashMap<Integer, SearchResultCollector>();
	}
	
	private OnnxRegistry(String OnnxfolderPath)
	{
		super();
		_onnxFolder = OnnxfolderPath;
	}
	
	
	public static OnnxRegistry getInstance()
	{
		return singletonRegistryInstance;
	}
	
	static public SearchResultCollector saveOnnx(ModelProto model, String projectId)
	{
		String onnxname = "o" + idCounter++ + ".onnx";
		File file = new File(_onnxFolder);
		if (!file.exists()) 
		{
            System.out.print("No Folder:" + _onnxFolder);
            file.mkdir();
            System.out.println("Folder created");
        }
	
		file = new File(_onnxFolder + "/onnx");
		if (!file.exists()) 
		{
            System.out.print("No Folder:" + _onnxFolder + "/onnx");
            file.mkdir();
            System.out.println("Folder created");
        }
		
		ONNXFileWorker.writeModel(model, _onnxFolder + "/onnx/" + onnxname);		
       
		ObjectId id = MongoDBTest.createAlgorithmConfiguration(projectId,  _onnxFolder + "/onnx/" + onnxname);
		SearchResultCollector srCol = new SearchResultCollector();
		
		srCol.id = id.toString();
		srCol.onnxName =  _onnxFolder + "/onnx/" + onnxname;
		return srCol;
        
        //TODO: db entry - get id, create resultCollector instance and 
	}
	
	public void addEntry(int GenotypeUniquecode,SearchResultCollector resultCollector)
	{
		if(hasEntry(new Integer(GenotypeUniquecode)))
			System.err.println("Duplicate Entry?");
		
		Register.put(new Integer(GenotypeUniquecode), resultCollector);
	}
	
	public Boolean hasEntry(int GenotypeUniquecode)
	{
		return Register.containsKey(new Integer(GenotypeUniquecode));
	}
	
	public SearchResultCollector getEntry(int GenotypeUniquecode)
	{
		return Register.get(new Integer(GenotypeUniquecode));
	}
}
