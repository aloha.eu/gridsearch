package nl.uva.aloha.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import io.jenetics.Genotype;
import nl.uva.aloha.genetic.LayerGene;

public class SatelliteEvaluator 
{
	
	public static String _pythonscriptFolder = "/satellite_tools.py";
	
	private String _accuracyEvaluatorName = "training";
	private String _hardwareEvaluatorName = "power_perf";
	private String _securityEvaluatorName = "security";
	private String _rpiEvaluatorName = "rpi";
	
	public static String projectId;

	public SatelliteEvaluator()
	{
		
	}
	
	public SatelliteEvaluator(String pythonscriptFolderPath)
	{
		super();
		_pythonscriptFolder = pythonscriptFolderPath;
	}
	
	public SearchResultCollector evaluateAccuracy(Genotype<LayerGene> genotype)
	{
		SearchResultCollector srCol = OnnxRegistry.getInstance().getEntry(System.identityHashCode(genotype));		
		
		callPythonFor(_accuracyEvaluatorName, srCol.id);
		
		srCol.accuracy = new Double(MongoDBTest.retrieveAlgorithmConfiguration(srCol.id, "validation_accuracy"));
		//System.out.println("acc: " + srCol.toString());
		return srCol;
	}
	
	
	
	public SearchResultCollector evaluateHardware(Genotype<LayerGene> genotype)
	{
		SearchResultCollector srCol = OnnxRegistry.getInstance().getEntry(System.identityHashCode(genotype));		
		
		try
		{
			
			callPythonFor(_hardwareEvaluatorName, srCol.id);
			//System.out.println("before_power_perf: ");// + srCol.toString());
			
			srCol.performance  = new Double(MongoDBTest.retrieveAlgorithmConfiguration(srCol.id, "performance"));
			srCol.energy = new Double(MongoDBTest.retrieveAlgorithmConfiguration(srCol.id, "energy"));
			srCol.memory = new Double(MongoDBTest.retrieveAlgorithmConfiguration(srCol.id, "memory"));
			srCol.processors= new Double(MongoDBTest.retrieveAlgorithmConfiguration(srCol.id, "processors")).intValue();
				
			//System.out.println("power_perf: ");// + srCol.toString());
			return srCol;
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
			return srCol;
		}
		
	}
	
	
	
	
	private String callPythonFor(String callName, String id)
	{
		
		File tempFile = new File("");
        String pythonScriptPath = tempFile.getAbsolutePath() + _pythonscriptFolder;
        		
		String[] cmd = new String[5];
        cmd[0] = "/usr/bin/python3";
        cmd[1] = pythonScriptPath;
        cmd[2] = callName;
        cmd[3] = "-i=" + projectId;
        cmd[4] = "-a=" + id;
       
	    try
	    {
	    	 Runtime rt = Runtime.getRuntime();
	         Process pr = rt.exec(cmd);
	         BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
             String line = "";
             String pythonScriptResult = "";
             while((line = bfr.readLine()) != null) {
                 pythonScriptResult+=line;
             }
             //if(!pythonScriptPath.isEmpty())
            //	 System.out.println("Error: " + pythonScriptResult);
             return pythonScriptResult;
	    }
       
        catch(Exception e)
	    {
        	System.err.println(e.getMessage());
        	return "";
	    }
	}

}
