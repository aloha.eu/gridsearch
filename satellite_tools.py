# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function


# the following two imports are needed for create a new connection

#from db.mongo import MongoConnector

#from rq import Queue
#from worker import *

#from models.models import *

from bson import ObjectId

from uuid import uuid4

import requests

import os
import argparse

import logging

#from redisConnector import RedisConnector

CB_WAIT_TIME = 5
SHARED_DATA_FOLDER = "/opt/data/"
LOGGING_FOLDER = os.path.join(SHARED_DATA_FOLDER, "experiments/prj_%s/dse_engine_logs/") # to be completed with the project_id



def barrier_cb (callback_id):

    redisc=RedisConnector()
    logging.info ("Waiting for callback (id: %s)" % callback_id)
    #wait for results. Query the redis DB for the exisistence of the callback_id key
    import time
    done = 0
    while done==0:
      if redisc.conn.exists(callback_id):
        #print ("Waiting for callback (id: %s)" % callback_id)
        time.sleep(CB_WAIT_TIME)
      else:
        done = 1

    logging.info ("Callback caugth.")

def get_callback_id ():
    callback_id = uuid4()
    redisc=RedisConnector()
    try:
        redisc.conn.set(callback_id, '1')
        logging.info ("Callback_id: %s"%str(callback_id))
    except:
        logging.error ("ERROR: Unable to set a callback id in the REDIS DB.")

    return callback_id





#  ██████╗ ███████╗██████╗ ███████╗    ███████╗██╗   ██╗ █████╗ ██╗         ██╗    ██╗██████╗  █████╗ ██████╗
#  ██╔══██╗██╔════╝██╔══██╗██╔════╝    ██╔════╝██║   ██║██╔══██╗██║         ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  ██████╔╝█████╗  ██████╔╝█████╗      █████╗  ██║   ██║███████║██║         ██║ █╗ ██║██████╔╝███████║██████╔╝
#  ██╔═══╝ ██╔══╝  ██╔══██╗██╔══╝      ██╔══╝  ╚██╗ ██╔╝██╔══██║██║         ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#  ██║     ███████╗██║  ██║██║         ███████╗ ╚████╔╝ ██║  ██║███████╗    ╚███╔███╔╝██║  ██║██║  ██║██║
#  ╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝         ╚══════╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝     ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝

def power_perf_eval_start (project_id=None, algorithm_id=None):

    host            = "power_perf:5000"
   # project_id      = kwargs.get("projectID", None)
   # algorithm_id    = kwargs.get("algorithm_id", None)



    #get a callback id
    callback_id = get_callback_id()


    url='http://%s/api/power_performance_evaluations?callback_id=%s'%(host,callback_id)
    data={
    "project_id":str(project_id),
    "algorithm_id":str(algorithm_id),
    "architecture_id":str(project_id) # TODO: remove this parameter. project_id is passed only to prevent restAPI errors, the architecture id is no longer required.
    }

    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))

    r = requests.post(url,json=data)

    logging.info("Response status code %s" % r.status_code )

    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")


    try:
        job_id = json_resp['job_id']
    except KeyError:
        logging.error ("There is no job_id key in the response.")
        logging.error (json_resp)



    return callback_id, job_id


def power_perf_eval_getresults (**kwargs):
    host                       = "power_perf:5000"
    #project_id                 = kwargs.get("projectID", None)
    algorithm_id               = kwargs.get("algorithm_id", None)

    '''
    #get results
    if r.json()['message']== 'finished':
        api='http://%s/api/eval_perf?algorithm_configuration_id=%s&project_id=%s&architecture_id=%s'%(host,algorithm_configuration_id, project_id, architecture_id)

        r = requests.get(api)

        print(r.json())
    elif r.json()['message']== 'failed':
        return 'failed'
    '''
    print ("perf eval results")


def power_perf_eval_getstatus (**kwargs):
    host                       = "power_perf:5000"
    project_id                 = kwargs.get("projectID", None)
    algorithm_id               = kwargs.get("algorithm_id", None)

    '''
    #get results
    if r.json()['message']== 'finished':
        api='http://%s/api/eval_perf?algorithm_configuration_id=%s&project_id=%s&architecture_id=%s'%(host,algorithm_configuration_id, project_id, architecture_id)

        r = requests.get(api)

        print(r.json())
    elif r.json()['message']== 'failed':
        return 'failed'
    '''
    print ("perf eval status")



#  ██████╗  █████╗ ██████╗ ███████╗██╗███╗   ███╗    ██╗███╗   ██╗███████╗███████╗    ██╗    ██╗██████╗  █████╗ ██████╗
#  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██║████╗ ████║    ██║████╗  ██║██╔════╝██╔════╝    ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  ██████╔╝███████║██████╔╝███████╗██║██╔████╔██║    ██║██╔██╗ ██║█████╗  █████╗      ██║ █╗ ██║██████╔╝███████║██████╔╝
#  ██╔═══╝ ██╔══██║██╔══██╗╚════██║██║██║╚██╔╝██║    ██║██║╚██╗██║██╔══╝  ██╔══╝      ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#  ██║     ██║  ██║██║  ██║███████║██║██║ ╚═╝ ██║    ██║██║ ╚████║██║     ███████╗    ╚███╔███╔╝██║  ██║██║  ██║██║
#  ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝╚═╝     ╚═╝    ╚═╝╚═╝  ╚═══╝╚═╝     ╚══════╝     ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝
#


def parsim_start (project_id=None, algorithm_id=None):



    host                       = "parsim_inference:5000"
   # project_id                 = kwargs.get("projectID", None)
   # algorithm_id    = kwargs.get("algorithm_configuration_id", None)


    #get a callback id
    callback_id = get_callback_id()

    # start refinement parsimonious inference
    url='http://%s/api/rpi?callback_id=%s'%(host,callback_id)
    data={"project_id":str(project_id),
          "algorithm_id":str(algorithm_id),
          "method":"string",
          "network":"string",
          "method_params": 0}

    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))

    r = requests.post(url,json=data)




    logging.info("Response status code %s" % r.status_code )

    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")


    try:
        job_id = json_resp['id'] #TODO the rpi tool returns id instead of job_id
    except KeyError:
        logging.error ("There is no job_id key in the response.")
        logging.error (json_resp)
        job_id = None


    return callback_id, job_id


def parsim_getresults (**kwargs):

    host                       = "parsim_inference:5000"
    project_id                 = kwargs.get("projectID", None)
    algorithm_id = kwargs.get("algorithm_configuration_id", None)


    #get results
    url='http://%s/api/rpi/{%s}?'%(host,job_id)

    r = requests.get(url)


def parsim_getstatus (**kwargs):

    host                       = "parsim_inference:5000"
    project_id                 = kwargs.get("projectID", None)
    algorithm_id = kwargs.get("algorithm_configuration_id", None)


    #get results
    url='http://%s/api/rpi/{%s}?'%(host,job_id)

    r = requests.get(url)





# ████████╗██████╗  █████╗ ██╗███╗   ██╗██╗███╗   ██╗ ██████╗     ██╗    ██╗██████╗  █████╗ ██████╗
# ╚══██╔══╝██╔══██╗██╔══██╗██║████╗  ██║██║████╗  ██║██╔════╝     ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#    ██║   ██████╔╝███████║██║██╔██╗ ██║██║██╔██╗ ██║██║  ███╗    ██║ █╗ ██║██████╔╝███████║██████╔╝
#    ██║   ██╔══██╗██╔══██║██║██║╚██╗██║██║██║╚██╗██║██║   ██║    ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#    ██║   ██║  ██║██║  ██║██║██║ ╚████║██║██║ ╚████║╚██████╔╝    ╚███╔███╔╝██║  ██║██║  ██║██║
#    ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═══╝ ╚═════╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝


def training (project_id=None, algorithm_id=None):



    host            = "training:5000"
   # project_id      = kwargs.get("projectID", None)
   # algorithm_id    = kwargs.get("algorithm_configuration_id", None)


    #get a callback id
    callback_id = get_callback_id()

  # start a new training

    url='http://%s/api/training_call?project_id=%s&designpoint_id=%s&callback_id=%s'%(host, project_id, algorithm_id, callback_id )
    logging.debug("Request URL %s" % url)

    try:
      r = requests.post(url) #TODO: the training tool gets the parameters in the url. Fix it passing them in the body
    except:
      logging.error ("Connection error! Ensure training module is up.")
      return 1

    logging.info("Response status code %s" % r.status_code )


    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")



    return callback_id

def training_getresults (**kwargs):

    host                       = "training:5000"
    project_id                 = kwargs.get("projectID", None)
    algorithm_id = kwargs.get("algorithm_configuration_id", None)

    api='http://%s/api/training_call?project_id=%s&designpoint_id=%s'%(host, project_id, algorithm_id )
    r = requests.post(api)

def training_getstatus (**kwargs):

    host                       = "training:5000"
    project_id                 = kwargs.get("projectID", None)
    algorithm_id = kwargs.get("algorithm_configuration_id", None)

    api='http://%s/api/training_call?project_id=%s&designpoint_id=%s'%(host, project_id, algorithm_id )
    r = requests.post(api)



#  ███████╗███████╗ ██████╗██╗   ██╗██████╗ ██╗████████╗██╗   ██╗    ███████╗██╗   ██╗ █████╗ ██╗
#  ██╔════╝██╔════╝██╔════╝██║   ██║██╔══██╗██║╚══██╔══╝╚██╗ ██╔╝    ██╔════╝██║   ██║██╔══██╗██║
#  ███████╗█████╗  ██║     ██║   ██║██████╔╝██║   ██║    ╚████╔╝     █████╗  ██║   ██║███████║██║
#  ╚════██║██╔══╝  ██║     ██║   ██║██╔══██╗██║   ██║     ╚██╔╝      ██╔══╝  ╚██╗ ██╔╝██╔══██║██║
#  ███████║███████╗╚██████╗╚██████╔╝██║  ██║██║   ██║      ██║       ███████╗ ╚████╔╝ ██║  ██║███████╗
#  ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝   ╚═╝      ╚═╝       ╚══════╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝
#



def security_eval (project_id=None, algorithm_id=None):

    #print (kwargs)


    host                       = "security:5000"


    #get a callback id
    callback_id = get_callback_id()



    req_body = {
    "dataset"             : project_id, #TODO: Since there is only one dataset for each project we can use the project_id to retrieve the Dataset. The API must be edited to replace the field name
    "trained-model"       : algorithm_id,
    "performance-metric"  : "classification-accuracy",
    "perturbation-type"   : "max-norm",
    "perturbation-values" : [0, 0.01, 0.02, 0.05, 0.1]
    }

    # start Security evaluation
    url = 'http://%s/api/security_evaluations?callback_id=%s'%(host,callback_id)


    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(req_body))


    r = requests.post(url, json=req_body)

    logging.info("Response status code %s" % r.status_code )

    #print (r.text)

    job_id = (r.text)[1:-2] #TODO: security tool doesn't return a json. fix it.

    '''
    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")


    try:
        job_id = json_resp['job_id']
    except KeyError:
        logging.error ("There is no job_id key in the response.")
        logging.error (json_resp)
    '''


    return callback_id, job_id


def security_eval_getresults(job_id = None, algorithm = None):

    logging.info("Retrieving the security results..." )

    host                       = "security:5000"
    job_id = kwargs.get("job_id", None)

    url = 'http://%s/api/security_evaluations/%s/output'%(host, job_id)

    logging.debug("Request URL %s" % url)

    r=requests.get(url)

    logging.info("Response status code %s" % r.status_code )


    try:
        resp_json = r.json()
    except:
        logging.error ("No JSON returned")

#try:

    # connect to the DB
    #mongo = MongoConnector()
    #print (resp_json)
    algorithm.sec_level = resp_json["sec-level"];
    algorithm.sec_value = resp_json["sec-value"];
    algorithm.sec_curve = resp_json["sec-curve"];
    algorithm.save()
    logging.info("Security values saved in DB. Argorithm id: %s" % algorithm.get_id() )
#    except:
#        logging.error ("ERROR: Access to bd failed.")


def security_eval_getimage(job_id = None, algorithm = None):

    logging.info("Retrieving the security results image..." )

    host                       = "security:5000"
    job_id = kwargs.get("job_id", None)

    url = 'http://%s/api/security_evaluations/%s/output/stored'%(host, job_id)

    logging.debug("Request URL %s" % url)

    r=requests.get(url)

    logging.info("Response status code %s" % r.status_code )



#try:

    # connect to the DB
    #mongo = MongoConnector()
    #print (resp_json)
    algorithm.security_img_path = r.text[1:-2]; #TODO security tool doesn't return a json
    algorithm.save()
    logging.info("Security image path saved in DB. Argorithm id: %s" % algorithm.get_id() )
#    except:
#        logging.error ("ERROR: Access to bd failed.")

def security_eval_getstatus(job_id = None):


    host                       = "security:5000"

    url = 'http://%s/api/security_evaluations/%s'%(host, job_id)

    r=requests.get(url)


    print (r.status_code)

    try:
        print (r.json())
    except:
        print ("No JSON returned")


    print ('Add to onnx class')
    
  

#  ███╗   ███╗ █████╗ ██╗███╗   ██╗
#  ████╗ ████║██╔══██╗██║████╗  ██║
#  ██╔████╔██║███████║██║██╔██╗ ██║
#  ██║╚██╔╝██║██╔══██║██║██║╚██╗██║
#  ██║ ╚═╝ ██║██║  ██║██║██║ ╚████║
#  ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
             
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Satellite tools API')
    parser.add_argument('tool', type=str, choices=['rpi', 'power_perf', 'security', 'training'], help='Select the tool to be executed.')
    parser.add_argument('-i', '--project',       default=None, type=str, help='The id of the current project')
    parser.add_argument('-a', '--algorithm',     default=None, type=str, help='The id of the algorithm configuration to be evaluated')
    args = parser.parse_args()
    
    #redisc=RedisConnector()
    #redisc.conn.set("a", '1')
    #exit(0)
    if args.tool == "rpi":
        parsim_cb, job_id = parsim_start (project_id=args.project, algorithm_id=args.algorithm)
        barrier_cb(parsim_cb)
        
    elif args.tool == "power_perf":
        #power_perf_eval_cb, job_id=power_perf_eval_start (project_id=args.project, algorithm_id=args.algorithm)
        
        #barrier_cb(power_perf_eval_cb)
        import json
        d = json.loads('{ "id": 0, "execution_time": 0.002737152, "energy": 15.926511128743488, "memory": 876872.0, "processors": 2 }')
        
        print (d)
    
    elif args.tool == "security":
        security_eval_cb, job_id = security_eval (project_id=args.project, algorithm_id=args.algorithm)
        barrier_cb(security_eval_cb)
        
        security_eval_getresults(job_id = job_id, algorithm = algorithm_configuration)

        security_eval_getimage  (job_id = job_id, algorithm  = algorithm_configuration)
    
    elif args.tool == "training":
        #training_cb = training (project_id=args.project, algorithm_id=args.algorithm)
        #barrier_cb(training_cb) # wait for the end of the training.
        print ("a")

